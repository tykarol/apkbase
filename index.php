<?
ob_start();

define('BASE_URL', 'http://'.$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF']).'/');
?>
<html>
<head>
<meta charset="utf-8" />
<title>Aplikacje Na Androida</title>
</head>
<body>
<style>
*{
	margin: 0;
	padding: 0;
	border: 0;
}
input{
	border: 1px solid #CECECE;
	height: 25px;
	vertical-align: middle;
	background: #ffffff;
	padding: 0 3px;
}
a{
	text-decoration: none;
	color: #000000;
}
a:hover{
	text-decoration: none;
}
body{
	background-image:url(img/background-stripes.gif);
	font-family: 'Droid Sans', Arial, sans-serif;
	font-size: 10px;
}
#container{
	width: 960px;
	margin: 10px auto;
	background: #ffffff;
	padding: 10px;
}
#footer{
	padding-top: 10px;
}
#left{
	float: left;
}
#right{
	float: right;
	text-align:right;
}
.clear{
	display: block;
	clear: both;
}
div.notify{
	margin: 5px 0;
	padding: 15px 10px;
	color: #D8000C;
	background-color: #FFBABA;
	border: 1px solid;
	text-align: center;
	font-size: 12px;
}
.list_one{
	min-height: 110px;
	display: block;
	border-bottom: 1px solid #CECECE; 
	padding: 10px 5px;
}
.list_one a:hover{
	text-decoration: underline;
}
.list_one h1, .list_one h1 a{
	font-size: 14px;
	color: #414242;
	font-weight: bold;
}
.list_one span{
	color: #8E8E8E;
	font-size: 10px;
	text-transform: uppercase;
}
.list_one small{
	color: #8E8E8E;
	font-size: 9px;
}
.list_one table td{
	padding-left: 5px;
	vertical-align: top;
}
.list_one div.screen{
	
}
.list_one div.files{
	padding-top: 5px;
	margin-top: 5px;
	border-top: 1px solid #CECECE; 
}
.list_one div.files_one{
	color: #000000;
	font-size: 12px;
	padding-bottom: 5px;
}
.list_one div.files_one a{
	color: #8E8E8E;
}
.list_one div.files_one .desc{
	color: #8E8E8E;
	font-size: 10px;
	padding-left: 20px;
}.list_one div.files_one .size{
	color: #8E8E8E;
	font-style: italic;
}
span.new{
	color: red;
	font-size: 14px;
	vertical-align: super;
}
#qr{
	position: fixed;
	top: 10px;
	left: 0px;
	width: 125px;
	padding: 10px;
	background: #ffffff;
	text-align: center;
	display: none;
}
</style>
<script type="text/javascript">
/* <![CDATA[ */
function qrcode( url, id )
{
	var el_qr 		= document.getElementById('qr');
	var el_download = document.getElementById('qr_download');
	var el_market 	= document.getElementById('qr_market');
	var el_sms 		= document.getElementById('qr_sms');
	
	var url_download= 'https://chart.googleapis.com/chart?chs=120x120&cht=qr&chld=L|0&choe=UTF-8&chl=' + url;
	var url_market 	= 'https://chart.googleapis.com/chart?chs=120x120&cht=qr&chld=L|0&choe=UTF-8&chl=market://search?q=pname%3A' + id;
	var url_sms 	= 'https://chart.googleapis.com/chart?chs=120x120&cht=qr&chld=L|0&choe=UTF-8&chl=SMSTO%3A%3A' + url;
	
	if( el_download.src == url_download && qr.style.display != 'none' )
	{
		qr.style.display = 'none';
	}
	else
	{
		el_download.src = url_download;
		el_market.src 	= url_market;
		el_sms.src 		= url_sms;
		
		qr.style.display = 'block';
	}
	
	//alert( url );
}

function qr_close( )
{
	var el_qr 		= document.getElementById('qr');
	qr.style.display = 'none';
}
/*]]> */
</script>
<div id="page">
	<div id="container">
		<?
			$users = array(
						'user1' 	=> 'asd123',
						'user2' 	=> 'zxc456',
			);
			
			$isLogin = false;
			
			$login 		= $_GET['login'];
			$pass 		= $_GET['pass'];
			$notify 	= $_GET['notify'];
			$delete 	= $_GET['delete'];
			$nos 		= $_GET['nos'];
			$cc 		= $_GET['cc'];
			
			$apkCount 	= 0;
			$fileCount 	= 0;
			$sizeCount 	= 0;
			
			if( isset($login) && !empty($login) && isset($pass) && !empty($pass) && isset($users[ $login ]) && $users[ $login ] == $pass )
			{
				$isLogin = true;
			}
			else
			{
				$login 	= '';
				$pass 	= '';
			}
		
			function getCURL( $url )
			{
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, CURLOPT_CONNECTTIMEOUT_val);
				curl_setopt($ch, CURLOPT_TIMEOUT, 30);
				curl_setopt($ch, CURLOPT_HEADER, 0);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
				curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
				$headers = array('Accept-Language: pl,en-us;q=0.7,en;q=0.3',
								 'Accept-Charset: ISO-8859-2,utf-8;q=0.7,*;q=0.7');
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
				if(substr($url,0,5) == 'https')
				{
				  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
				  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
				}
				 
				$result = curl_exec($ch);
				curl_close ($ch);
				
				return $result;
			}
			
			function format_bytes($bytes) {
			   if ($bytes < 1024) return $bytes.'B';
			   elseif ($bytes < 1048576) return round($bytes / 1024, 2).'kB';
			   elseif ($bytes < 1073741824) return round($bytes / 1048576, 2).'MB';
			   elseif ($bytes < 1099511627776) return round($bytes / 1073741824, 2).'GB';
			   else return round($bytes / 1099511627776, 2).'TB';
			}	

			function array_msort($array, $cols)
			{
				$colarr = array();
				foreach ($cols as $col => $order) {
					$colarr[$col] = array();
					foreach ($array as $k => $row) { $colarr[$col]['_'.$k] = strtolower($row[$col]); }
				}
				$eval = 'array_multisort(';
				foreach ($cols as $col => $order) {
					$eval .= '$colarr[\''.$col.'\'],'.$order.',';
				}
				$eval = substr($eval,0,-1).');';
				eval($eval);
				$ret = array();
				foreach ($colarr as $col => $arr) {
					foreach ($arr as $k => $v) {
						$k = substr($k,1);
						if (!isset($ret[$k])) $ret[$k] = $array[$k];
						$ret[$k][$col] = $array[$k][$col];
					}
				}
				return $ret;
			}
			
			$dir 		= 'apk/';
			$dirCache 	= 'cache/';
			$ext 		= '(apk|zip|rar)';
			$textNew 	= '<span class="new">NEW</span>';
			
			$marketPL 	= 'https://market.android.com/details?hl=pl&id=';
			$marketEN 	= 'https://market.android.com/details?hl=en&id=';
			
			$aNotify 	= array(
								'cache' 				=> 'Cache został wyczyszczony O.o',
								'permission' 			=> 'Musisz być zalogowany aby wykonać wybraną akcję ;(',
								'upload_incorrectname' 	=> 'Nieprawidłowy format nazwy pliku -.-',
								'upload_fileexist' 		=> 'Plik o tej nazwie już istnieje ;P',
								'upload_ok' 			=> 'Plik został załadowany poprawnie ^^',
								'upload_err' 			=> 'Wystąpił błąd podczas uploadu pliku :/',
								'app_delete' 			=> 'Aplikacja została usunięta ; )',
						);
			
			if( isset($delete) && !preg_match('/(..\/|..\\\)/', $delete) && is_file($dir.$delete) )
			{
				if( $isLogin == true )
				{
					@unlink($dir.$delete);
					
					preg_match('/^(.*?)_v(.*?).'.$ext.'$/', $delete, $matches);
					@unlink($dirCache.$matches[1]);
					
					header('Location: ?login='.$login.'&pass='.$pass.'&notify=app_delete');
				}
				else
				{
					header('Location: ?login='.$login.'&pass='.$pass.'&notify=permission');
				}
			}
			
			if( isset($_FILES['file']) )
			{
				if( $isLogin == true )
				{
					$uName = basename($_FILES['file']['name']);
					$uFile = $dir.$uName;
					
					if( !preg_match('/^(.*?)_v(.*?).'.$ext.'$/', $uName) )
					{
						$n = 'upload_incorrectname';
					}
					elseif( is_file($uFile) )
					{
						$n = 'upload_fileexist';
					}
					elseif( move_uploaded_file($_FILES['file']['tmp_name'], $uFile) ) 
					{
						$n = 'upload_ok';
					} 
					else 
					{
						$n = 'upload_err';
					}
					
					header('Location: ?login='.$login.'&pass='.$pass.'&notify='.$n);
				}
				else
				{
					header('Location: ?login='.$login.'&pass='.$pass.'&notify=permission');
				}
			}
			
			if( isset($cc) && $cc == 'yes' )
			{
				array_map('unlink', glob($dirCache.'*'));
				
				header('Location: ?login='.$login.'&pass='.$pass.'&notify=cache');
			}
			
			if( isset($notify) && isset($aNotify[$notify]) && !empty($aNotify[$notify]) )
			{
				echo '<a href="?login='.$login.'&pass='.$pass.'"><div class="notify">'.$aNotify[$notify].'</div></a>';
			}
			
			$files = glob($dir.'*.*');
			$files = str_replace($dir, '', $files);
			
			$apk = array();
			
			if( is_array($files) && count($files) > 0 )
			{
				foreach( $files as $key => $val )
				{
					if( preg_match('/^(.*?)_v(.*?).'.$ext.'$/', $val, $matches) )
					{
						$file 	= $matches[0];
						$id 	= $matches[1];
						$ver 	= $matches[2];
						
						$apk[$id][] = array('id' => $id, 'ver' => $ver, 'file' => $file);
					}
				}
			}
			
			if( is_array($apk) && ($apkCount = count($apk)) > 0 )
			{
				$apkInfo = array();
				
				foreach( $apk as $key => $val )
				{
					$linkPL = $marketPL.$key;
					$linkEN = $marketEN.$key;
					
					$cache 	= $dirCache.$key;
					
					if( is_file($cache) && date('Y-m-d', filemtime($cache)) == date('Y-m-d') )
					{
						$page = file_get_contents($cache);
					}
					else
					{
						$page = getCURL($linkPL); // linkPL / linkEN - dane o aplikacji sÄ? pobierane ze strony o wybranym jezyku
						file_put_contents($cache, $page);
						chmod($cache, 0666);
					}
					$new = true;
					$fileList = array();
					$info = array();
					
					$info['link'] 		= $linkPL; // linkPL / linkEN - link do strony marketu w wybranej wersji jezykowej
					$info['id'] 		= $key;
					
					// Domyslne dane (w przypadku braku aplikacji w markecie)
					$info['name'] 		= $key;
					$info['dev'] 		= 'Other';
					$info['icon'] 		= 'img/icon.png';
					$info['date'] 		= '---';
					$info['ver'] 		= '---';
					$info['require'] 	= '---';
					$info['size'] 		= '---';
					$info['price'] 		= '---';
					
					preg_match('/<td class="doc-banner-title-container">(.*?)<\/td>/', $page, $name_dev);
					preg_match('/<h1 class="doc-banner-title">([^<]+)<\/h1>/', $name_dev[1], $name);
					preg_match('/<a[^>]*>([^<]+)<\/a>/', $name_dev[1], $dev);
					
					if( isset($name[1]) )
						$info['name'] 		= $name[1];
					if( isset($dev[1]) )
						$info['dev'] 		= $dev[1];
					
					preg_match('/<div class="doc-banner-icon">(.*?)<\/div>/', $page, $img);
					preg_match('/<img src="([^"]+)" \/>/', $img[1], $icon);
					
					if( isset($icon[1]) )
						$info['icon'] 		= $icon[1];

					preg_match('/<dl class="doc-metadata-list"[^>]*>(.*?)<\/dl>/', $page, $about);
					preg_match_all('/<dd[^>]*>(.*?)<\/dd>/', $about[1], $aboutList);
					
					$aboutList = array_map('trim', array_map('strip_tags', $aboutList[1]));
					
					if( isset($aboutList[1]) )
						$info['date'] 		= $aboutList[1];
					if( isset($aboutList[2]) )
						$info['ver'] 		= $aboutList[2];
					if( isset($aboutList[3]) )
						$info['require'] 	= $aboutList[3];
					if( isset($aboutList[6]) )
						$info['size'] 		= $aboutList[6];
					if( isset($aboutList[7]) )
						$info['price'] 		= $aboutList[7];
					
					if( !isset($name[1]) )
						$info['link'] 		= '#app_'.$info['id'];
						
					if( !isset($aboutList[1]) || preg_match('/(Zależy od urządzenia|Varies with device)/is', $aboutList[2]) )
						$new = false;
						
					if( preg_match('/.*?\((około|about) (.*?)\)/is', $aboutList[7], $price) )
						$info['price'] 		= '~'.$price[2];
						
					
					preg_match('/<div class="screenshot-carousel-content-container">(.*?)<\/div>/', $page, $pic);
					preg_match_all('/<img src="([^"]+)".*?\/>/', $pic[1], $screen);
					
					if( $nos == 1 )
						$info['screen'] = array();
					else
						$info['screen'] = $screen[1];
					
					$iFiles = array();
					
					foreach($val as $k => $v)
					{
						$fName = $v['file'];
						$fVer  = $v['ver'];
						$fFile = $dir.$fName;
						$fSize = format_bytes(filesize($fFile));
						
						if( $fVer >= $info['ver'] )
							$new = false;
						
						$fInfo['name'] = $fName;
						$fInfo['size'] = $fSize;
						$fInfo['file'] = $fFile;
						
						$iFiles[] = $fInfo;
						
						$fileCount++;
						$sizeCount += filesize($fFile);
					}
					
					$iFiles = array_msort($iFiles, array('name' => SORT_ASC));
					
					$info['files'] 	= $iFiles;
					$info['new'] 	= $new;
										
					/*
					id - app id in market
					name - app name
					dev - dev name
					icon - app icon
					new - new version in market?
					link - market link 
					date - app date
					ver - app version
					require - app require system
					size - app size
					price - app price
					screen (array) - screens array
					files (array) - array file on server
					*/
					$apkInfo[] = $info;
				}
				
				$apkInfo = array_msort($apkInfo, array('name' => SORT_ASC, 'dev' => SORT_ASC));
				
				foreach( $apkInfo as $i => $app )
				{
				
					if( count($app['screen']) > 0 )
					{
						$screens = array();
						
						$i = 1;
						foreach($app['screen'] as $k => $v)
						{
							if( $i > 4 ) break;
							
							$screens[] = '<a href="'.$v.'"><img src="'.$v.'" width="90px;" /></a> ';
							
							$i++;
						}
						
						$app['screen'] = implode('', $screens);
					}
					else
					{
						$app['screen'] = '';
					}

					if( count($app['files']) > 0 )
					{
						$fileList = array();
						
						foreach($app['files'] as $k => $v)
						{
							$fileList[] = '<div class="files_one">'.$v['name'].' <span class="size">('.$v['size'].')</span><br />
								<span class="desc"><a href="'.$v['file'].'">Download</a> | <a href="javascript:qrcode(\''.urlencode(BASE_URL.$v['file']).'\', \''.$app['id'].'\');">QR CODE</a> | <a href="?login='.$login.'&pass='.$pass.'&delete='.$v['name'].'" onclick="return confirm(\'Czy na pewno chcesz usunąć plik?\');">Delete</a></span></div>';
						}
						
						$app['files'] = implode('', $fileList);
					}
					else
					{
						$app['files'] = '';
					}
					
					
					echo '<div class="list_one" id="app_'.$app['id'].'">';
						echo '<table>
								<tr>
									<td width="120px;"><a href="#app_'.$app['id'].'"><img src="'.$app['icon'].'" style="width: 100px; height: 100px;"></a></td>
									<td width="46%">
										<h1><a href="'.$app['link'].'">'.$app['name'].'</a> '.($app['new']?$textNew:'').'</h1>
										<span>'.$app['dev'].'</span><br />
										<small>Data: '.$app['date'].', Wersja: '.$app['ver'].', Wymagania: '.$app['require'].', Rozmiar: '.$app['size'].', Cena: '.$app['price'].'</small>
										<div class="files">'.$app['files'].'</div>
									</td>
									<td width="41%">
										<div class="screen">'.$app['screen'].'</div>
									</td>
								</tr>
							</table>';
					echo '</div>';
				
				}
			}
			else
			{
				echo '<div class="notify">Brak aplikacji ;P</div>';
			}			
			
		?>
		
		<div id="footer">
			<div id="left" style="width:315px;">
				<form action="" method="GET">
					<input name="login" type="text" value="<?=$login ?>" placeholder="Login" style="width:120px;" />
					<input name="pass" type="password" value="<?=$pass ?>" placeholder="Hasło" style="width:120px;" />
					<input type="submit" value=" Zaloguj " />
				</form>
			</div>
			<div id="left" style="width:330px;text-align:center;font-size:12px;margin-top:5px;">
				<a href="?cc=yes&login=<?=$login ?>&pass=<?=$pass ?>">Aplikacji: <?=$apkCount ?> | Plików: <?=$fileCount ?> | Rozmiar: <?=format_bytes($sizeCount) ?></a>
			</div>
			<div id="right" style="width:315px;">
				<form action="?login=<?=$login ?>&pass=<?=$pass ?>" enctype="multipart/form-data" method="POST">
					<input type="file" name="file" />
					<input type="submit" value=" Dodaj " />
				</form>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	<div id="qr" onClick="qr_close( );">
		DOWNLOAD LINK<br />
		<img id="qr_download" src="" style="width:120px;height:120px;" /><br />
		<br />
		MARKET LINK<br />
		<img id="qr_market" src="" style="width:120px;height:120px;" /><br />
		<br />
		LINK IN SMS<br />
		<img id="qr_sms" src="" style="width:120px;height:120px;" /><br />
	</div>
</div>
</body>
</html>
<?
ob_end_flush();
?>	